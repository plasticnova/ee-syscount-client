use crate::{
    process::Process,
    settings::{Settings, System},
    Result,
};
use log::debug;
use std::{
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread::{self, JoinHandle},
    time::Duration,
};

#[derive(Debug)]
pub struct Worker {
    system: System,
    settings: Settings,
}

impl Worker {
    pub fn new(system: &System, settings: Settings) -> Self {
        Worker {
            settings: settings.clone(),
            system: system.clone(),
        }
    }

    pub fn child(&self, api_path: String, running: Arc<AtomicBool>) -> Result<JoinHandle<()>> {
        let system = self.system.clone();
        let settings = self.settings.clone();
        Ok(thread::spawn(move || {
            while running.load(Ordering::SeqCst) {
                debug!("Working system: {}", system.name.clone());
                match Process::system(system.clone(), api_path.clone(), settings.clone()) {
                    Ok(_) => {}
                    Err(_e) => {}
                }
                thread::sleep(Duration::from_secs(1))
            }
        }))
    }
}
