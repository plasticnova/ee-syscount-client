use thiserror::Error;

mod msg {
    pub const ERR_NOT_CONVERTIBLE_TO_UTF_8: &str = "Error: not convertible to UTF-8";
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("general error: {0}")]
    General(#[from] std::io::Error),
    #[error("settings error: {0}")]
    Settings(#[from] config::ConfigError),
    #[error("tesseract error: {0}")]
    Tesseract(#[from] leptess::tesseract::TessInitError),
    #[error("leptonica error: {0}")]
    Leptonica(#[from] leptess::leptonica::PixError),
    #[error("utf8 error: {0}")]
    Utf8(#[from] std::str::Utf8Error),
    #[error("parse int error: {0}")]
    ParseInt(#[from] std::num::ParseIntError),
    #[error("ctrlc error: {0}")]
    Ctrlc(#[from] ctrlc::Error),
    #[error("reqwest error: {0}")]
    Reqwest(#[from] reqwest::Error),
    #[error("{}: {:?}", msg::ERR_NOT_CONVERTIBLE_TO_UTF_8, 0)]
    NotConvertibleToUtf8(std::ffi::OsString),
    #[error("raster error: {:?}", 0)]
    Raster(raster::error::RasterError),
    #[error("Resource unavailable: {0}")]
    Resource(&'static str),
}

impl From<std::ffi::OsString> for Error {
    fn from(err: std::ffi::OsString) -> Self {
        Self::NotConvertibleToUtf8(err)
    }
}

impl From<raster::error::RasterError> for Error {
    fn from(err: raster::error::RasterError) -> Self {
        Self::Raster(err)
    }
}
