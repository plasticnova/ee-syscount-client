use crate::{
    error::Error,
    send,
    settings::{Settings, System},
    Result,
};
use leptess::{leptonica, tesseract::TessApi};
use log::{debug, error, trace, warn};
use raster::{
    editor, filter,
    interpolate::{resample, InterpolationMode},
    PositionMode,
};
use std::{ffi::OsStr, fs, path::Path};
use tempfile::tempdir;

#[derive(Debug, Default)]
pub struct Process {}

impl Process {
    pub fn system(system: System, api_path: String, settings: Settings) -> Result<()> {
        let process = Process::default();
        process.directory(system, api_path, settings)
    }

    fn directory(&self, system: System, api_path: String, settings: Settings) -> Result<()> {
        for entry in fs::read_dir(system.dir)? {
            let entry = entry?;
            let path = entry.path();
            if !path.is_dir() {
                let path_str = path.into_os_string().into_string()?;
                debug!(
                    "Worker: {} Detected file: {}",
                    system.name.clone(),
                    &path_str
                );
                let ext = Path::new(&path_str).extension().and_then(OsStr::to_str);
                if ext == Some("png") {
                    match self.image(path_str.clone(), api_path.clone()) {
                        Ok(i) => {
                            debug!("Worker {} Parsed int: {}", system.name.clone(), i);
                            if settings.clone().dry_run {
                                debug!("Detected dry run flag, not sending results upstream");
                            } else {
                                debug!("Sending results upstream");
                                send::count(system.name.clone(), i, settings.clone())?;
                            }
                        }
                        Err(e) => {
                            error!("{}", e);
                        }
                    }
                    fs::remove_file(&path_str)?;
                } else {
                    warn!("Only png files are supported");
                }
            }
        }
        Ok(())
    }

    fn image(&self, file: String, api_path: String) -> Result<u16> {
        let mut api = TessApi::new(Some(&api_path), "eng")?;
        let tmp_dir = tempdir()?;
        let count_file_buf = tmp_dir.path().join("count.png");
        let count_file = match count_file_buf.to_str() {
            Some(file) => file,
            None => {
                return Err(Error::Resource("Unable to form count file path string"));
            }
        };
        let mut img = raster::open(&file)?;
        resample(&mut img, 1808, 1029, InterpolationMode::Nearest)?;
        editor::crop(&mut img, 60, 50, PositionMode::TopLeft, 60, 830)?;
        filter::grayscale(&mut img)?;
        // filter::brightness(&mut img, 1.2)?;
        filter::sharpen(&mut img)?;
        raster::save(&img, &count_file)?;

        let pix = leptonica::pix_read(Path::new(&count_file))?;
        api.set_image(&pix);

        if api.get_source_y_resolution() <= 0 {
            api.set_source_resolution(600)
        }
        let text = api.get_utf8_text();
        trace!("Unparsed text: {}", text.clone()?.trim());
        let int: u16 = text?.trim().parse()?;
        drop(count_file);
        tmp_dir.close()?;
        Ok(int)
    }
}
