mod error;
mod process;
mod send;
mod settings;
mod worker;

use env_logger::Env;
use rust_embed::RustEmbed;
use settings::Settings;
use std::{
    fs::File,
    io::prelude::*,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread::JoinHandle,
};
use tempfile::tempdir;
use worker::Worker;

pub type Result<T> = std::result::Result<T, error::Error>;

#[derive(RustEmbed)]
#[folder = "assets/"]
struct Asset;

fn main() -> Result<()> {
    let settings = Settings::new()?;
    let log_lvl: &str;
    if settings.debug {
        log_lvl = "debug";
    } else {
        log_lvl = "warn";
    }
    env_logger::Builder::from_env(Env::default().default_filter_or(log_lvl)).init();

    let traineddata = match Asset::get("tessdata/eng.traineddata") {
        Some(data) => data,
        None => {
            return Err(error::Error::Resource(
                "Unable to load traineddata embedded asset",
            ));
        }
    };
    let tmp_dir = tempdir()?;
    let file_path = tmp_dir.path().join("eng.traineddata");
    let mut datafile = File::create(file_path)?;
    datafile.write_all(traineddata.as_ref())?;
    datafile.sync_data()?;
    let api_path = &tmp_dir.path().to_string_lossy();

    let running = Arc::new(AtomicBool::new(true));
    let r = running.clone();
    ctrlc::set_handler(move || {
        r.store(false, Ordering::SeqCst);
    })
    .expect("Error setting Ctrl-C handler");

    let mut children: Vec<JoinHandle<()>> = Vec::new();

    for system in &settings.systems {
        let worker = Worker::new(&system, settings.clone());
        let child = worker.child(api_path.to_string(), running.clone())?;
        children.push(child);
    }
    for child in children {
        child.join().expect("oops! the child thread panicked");
    }
    tmp_dir.close()?;
    Ok(())
}
