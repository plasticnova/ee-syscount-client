use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;

#[derive(Clone, Debug, Deserialize)]
pub struct Upstream {
    pub host: String,
    pub token: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct System {
    pub name: String,
    pub dir: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Settings {
    pub debug: bool,
    pub dry_run: bool,
    pub upstream: Upstream,
    pub systems: Vec<System>,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::new();

        s.merge(File::with_name("config.json").required(false))?;

        // Add in settings from the environment (with a prefix of EESYSCOUNT)
        // Eg.. `EESYSCOUNT_DEBUG=1` would set the `debug` key
        s.merge(Environment::with_prefix("eesyscount"))?;

        s.try_into()
    }
}
