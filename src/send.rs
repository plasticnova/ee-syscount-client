use crate::{settings::Settings, Result};
use reqwest::blocking::Client;
use serde::Serialize;

#[derive(Serialize)]
struct RecordRequest {
    access_token: String,
    system_name: String,
    system_count: u16,
}

const RECORD_ROUTE: &str = "/api/count";

pub fn count(system_name: String, system_count: u16, settings: Settings) -> Result<()> {
    let client = Client::new();
    let route = format!("{}{}", &settings.upstream.host, RECORD_ROUTE);
    let req = RecordRequest {
        access_token: settings.upstream.token,
        system_name,
        system_count,
    };
    let _res = client.post(&route).json(&req).send()?;
    Ok(())
}
